import glob
import os
import pathlib

from epics import caput

path = pathlib.Path(__file__).parent.absolute()
print(path)

file_names = glob.glob('*.csv')

for file_name in file_names:
    pv_name = file_name[:-4].replace("_", ":") + "-CalCSV"
    print(pv_name)
    caput(pv_name, str(path) + "/" + file_name)
